import os

from v1.tables_definition import tables_mapping

# NOTE: only uppercase variables are imported as configuration in the app
# Statement for enabling the development environment
DEBUG = False

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# DATABASE INFORMATION
ATHENA_DATABASE_NAME = 'cisco_challenge'
ATHENA_TABLE_MAPPING_DICT = tables_mapping
ATHENA_S3_OUTPUT = 's3://malware-query-results/'
ATHENA_S3_SOURCE = 's3://malware-data/'

ATHENA_CONNECTION_STRING = "{aws_access}::{aws_secret}::{aws_region}".format(
    aws_access=os.getenv('AWS_ACCESS_KEY', "AKIAJNL5UWOJBPVINRWA"),
    aws_secret=os.getenv('AWS_SECRET_KEY', "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"),
    aws_region=os.getenv('AWS_REGION', "us-west-2"))

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Flask-Restplus settings
SWAGGER_UI_DOC_EXPANSION = 'full'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
ERROR_404_HELP = False
BUNDLE_ERRORS = True

# Secret key for signing cookies
SECRET_KEY = b'\x81\x81"\xd13\x0e\xa4\xe1\x9e.\xdd\x932\xecj\x81@A\x01\xe8\x8c\x9a\x93\x93'
