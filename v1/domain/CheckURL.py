from v1.domain.VerifiedURL import VerifiedURL
from v1.app import athena

class CheckURL:

    def is_malicious(self, host, port, url, query_string=None):
        resp = athena.is_malicious(host, port, url, query_string)
        return VerifiedURL(host, port, url, query_string, resp)
