class VerifiedURL:

    def __init__(self, host, port, url, query_string=None, malicious=None):
        self.host = host
        self.port = port
        self.url = url[1:] if url.startswith('/') else url
        self.query_string = query_string
        self.malicious = malicious or False

    @property
    def url_path(self):
        if self.query_string:
            return '{host}:{port}/{url}?{query_string}'.format(host=self.host, port=self.port, url=self.url,
                                                               query_string=self.query_string)
        else:
            return '{host}:{port}/{url}'.format(host=self.host, port=self.port, url=self.url)
