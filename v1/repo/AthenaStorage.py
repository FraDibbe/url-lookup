import functools

from v1.repo.AthenaDataBase import AthenaDataBaseAPI


class AthenaStorage(AthenaDataBaseAPI):
    """
    Implements the Storage interface integrating it with Athena AWS Service
    """

    @functools.lru_cache(maxsize=32)
    def is_malicious(self, host, port, url, query_params=None):
        set_of_table_result_column = {'Count'}
        # query the db
        res = self.run_query(sql_query=self._build_check_malware_SQL(host, port, url, query_params))
        row_set = self.build_lst_of_dict(res, set_of_table_result_column)
        # If we got a result and the Count > 0 than there is at least one entry in the db, hence the url is malicious
        if len(row_set) == 1 and int(row_set[0]['Count']) > 0:
            return True
        return False

    def _build_check_malware_SQL(self, host, port, url, query_params=None):
        check_malware_SQL = "SELECT COUNT(*) as Count FROM malware " \
                            "WHERE hostname ='{host}'" \
                            " AND port ='{port}'" \
                            " AND url='{url}'"
        if query_params:
            check_malware_SQL += " AND query_param='{query_params}'"
        check_malware_SQL += ';'
        check_malware_SQL = check_malware_SQL.format(host=host, port=port, url=url, query_params=query_params)
        return check_malware_SQL
