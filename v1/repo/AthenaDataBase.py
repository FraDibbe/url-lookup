import time

import boto3


class ConnectionStringMalformed(Exception):
    pass


class AthenaNotInitializedError(Exception):
    pass


class AthenaDataBaseAPI:
    """
    Implements the APIs to easy created a Unique class using boto3, Athena AWS SDK
    """

    def __init__(self):
        self.db = None
        self.s3_source = None
        self.s3_output = None
        self._connection_string = None
        self.__client = None
        self.__init_called = False

    ###############################################################################################################
    # Class Methods, use BOTH of them to initialize (at application instantiation) the AthenaDataBaseAPI object
    ###########################################
    def init_db(self, db_name, s3_source, s3_output, connection_string):
        """
        This method must be called during Flask App initialization to create a Global singleton object.
        All the object instantiation, then, will share the same connection and database values with minimal overhead
        DO NOT CHANGE CLASS ATTRIBUTE AFTER INSTANTIATION
        """
        if not self.__init_called:
            self.db = db_name
            self.s3_source = s3_source
            self.s3_output = s3_output
            self._connection_string = connection_string
            self.__client = self.__create_athena_client()
            self.__init_called = True
        else:
            # raise AthenaNotInitializedError('Athena is already initialized!')
            print('Database already Initialized. Skipping.')

    ##############################################################################################
    # Public Methods (Subclasses welcome to use the safely)
    ###########################################
    def get_database_type(self):
        return type(self)

    def run_query(self, sql_query):
        response = self.__client.start_query_execution(
            QueryString=sql_query,
            QueryExecutionContext={
                'Database': self.db
            },
            ResultConfiguration={
                'OutputLocation': self.s3_output,
            }
        )
        return self.__wait_till_completed(response['QueryExecutionId'])

    def create_db(self):
        create_db_if_not_exists = 'CREATE DATABASE IF NOT EXISTS {}'.format(self.db)
        self.run_query(create_db_if_not_exists)

    def create_tables(self, tables_mapping):
        # verify a table mapping is a dictionary
        if type(tables_mapping) != dict:
            raise ValueError('Table Mapping must be a dictionary')

        # verify tables are created or create them in Athena
        for table_name, table_ddl in tables_mapping.items():
            table_ddl = table_ddl.format(db_name=self.db, table_name=table_name, s3_source_location=self.s3_source)
            self.run_query(table_ddl)

    def build_lst_of_dict(self, list_of_athena_raw_rows, set_of_dict_keys):
        def extract_value(raw_column):
            try:
                return raw_column['VarCharValue']
            except KeyError:
                raise NotImplementedError('Only "STRING" column type are supported in this version')

        # Extract column definition
        column_definition = list_of_athena_raw_rows[0]['Data']
        # build the reference between array position and keys
        key_position_mapping = {}
        for index, column in enumerate(column_definition):
            v = extract_value(column)
            if v not in set_of_dict_keys:
                raise ValueError(
                    '"{}" is not a valid column for the Raw Athena Result "{}"'.format(v, column_definition))
            key_position_mapping[index] = v

        return_lst_dict = []

        # now that i build the key_position_mapping is possible to build the lst_of_dict
        for elem in list_of_athena_raw_rows[1:]:
            data_row = elem['Data']
            di = {}
            for index, raw_column in enumerate(data_row):
                v = extract_value(raw_column)
                di[key_position_mapping[index]] = v
            return_lst_dict.append(di)

        return return_lst_dict

    #######################################################################################
    # Class __Masked__ method to create client (SHOULD'T be used by subclassed)
    ###########################################
    def __create_athena_client(self):
        try:
            aws_acc, aws_secret, aws_region = self._connection_string.split('::')
        except ValueError:
            raise ConnectionStringMalformed(
                'Connection String expected to be in format AWS_ACCESS_KEY::AWS_SECRET_KEY::AWS_REGION')

        return boto3.client('athena',
                            aws_access_key_id=aws_acc,
                            aws_secret_access_key=aws_secret,
                            region_name=aws_region
                            )

    def __wait_till_completed(self, execution_id):
        def get_status(execution_id):
            return self.__client.get_query_execution(QueryExecutionId=execution_id)['QueryExecution']['Status']['State']

        retries = 0
        while get_status(execution_id) not in ['SUCCEEDED', 'FAILED'] and retries <= 7:
            retries += 1
            time.sleep(0.5)
        if get_status(execution_id) == 'SUCCEEDED':
            res = self.__client.get_query_results(QueryExecutionId=execution_id)
            if res['ResponseMetadata']['HTTPStatusCode'] == 200:
                return res['ResultSet']['Rows']
        return None
