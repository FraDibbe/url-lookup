__malware_table = """
CREATE EXTERNAL TABLE IF NOT EXISTS {db_name}.{table_name} (
    `hostname` string,
    `port` string,
    `url` string,
    `query_param` string
     )
     ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
     WITH SERDEPROPERTIES (
      'serialization.format' = ',',
      'field.delim' = ','
     ) LOCATION '{s3_source_location}'
     TBLPROPERTIES ('has_encrypted_data'='false');
"""

tables_mapping = {
    'malware': __malware_table,
}
