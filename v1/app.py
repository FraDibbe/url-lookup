from flask import Flask
from flask_caching import Cache

from v1.repo.AthenaStorage import AthenaStorage

# -- global cache -- because of cache.init_app(flask_app) cache can distinguish between multiple flask apps
cache = Cache(config={'CACHE_TYPE': 'simple'})

# -- global database -- because of cache.init_db() databse CANNOT distinguish between multiple flask apps
# hence each flask app uses the same global shared DB access layer (not a big problem in this case,
# however for complex scenario it might be worth to create an athena.init_app(flask_app) method)
athena = AthenaStorage()


def create_app(config_filename):
    """
    Is the Flask Application Factory. It creates and set up an App using configurations passed as parameters
    :param config_filename: a string containing the relative path of a configuration file which will be applied
    while building the App
    :return: a Flask Application object, correctly build and initialized
    """
    app = Flask(__name__)
    app.config.from_object(config_filename)

    # Route's Cache
    cache.init_app(app)

    # Init Athena DB
    athena.init_db(db_name=app.config['ATHENA_DATABASE_NAME'],
                   connection_string=app.config['ATHENA_CONNECTION_STRING'],
                   s3_source=app.config['ATHENA_S3_SOURCE'],
                   s3_output=app.config['ATHENA_S3_OUTPUT']
                   )

    # Create DB and Schema (if not yet created on the cloud )
    athena.create_db()
    athena.create_tables(tables_mapping=app.config['ATHENA_TABLE_MAPPING_DICT'])

    # Configure the Application attaching Blueprints and Routes
    configure_blueprints_and_routes(app)

    # return the Flask Application build from the factory
    return app


def configure_blueprints_and_routes(app):
    """
    Attaches Blueprints and Routes to the application object
    :return: None
    """

    # ################# BLUEPRINTS IMPORT & REGISTRATION
    from v1.entrypoints import apiBlueprint
    app.register_blueprint(apiBlueprint)
