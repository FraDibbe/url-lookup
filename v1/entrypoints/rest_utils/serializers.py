from flask_restplus import fields
from v1.entrypoints.urlAPI import challenge

# -------------------------------  PARSERs /STORAGE
url_safety = challenge.model('URL Safety', {
        'url_path': fields.String(),
        'malicious': fields.Boolean()
})