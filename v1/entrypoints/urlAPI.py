from flask import request
from flask_restplus import Resource, Namespace

challenge = Namespace('', description='URL Checker API')

from v1.entrypoints.rest_utils.serializers import url_safety
from v1.domain.CheckURL import CheckURL


@challenge.route('/<string:hostname_and_port>/<path:original_path>')
class ChallengeStored(Resource):

    @challenge.response(200, '')
    @challenge.response(400, 'Incorrect hostname, port, url or query string')
    @challenge.marshal_with(url_safety)
    # @cache.cached(timeout=60)
    def get(self, hostname_and_port, original_path):
        """
        Returns JSON indicating if the URL is safe to access or not

        Given an hostname and port and a url with query string the service answers with a JSON indicating if the
        URL is safe to access or not
        """
        query_string = ''
        try:
            host, port = hostname_and_port.split(':')
            query_string = request.query_string.decode("utf-8")
        except ValueError:
            return 'Incorrect Hostname or Query string format. Got {}{}?{}'.format(hostname_and_port, original_path,
                                                                                   query_string), 400
        url_path = '/' + original_path
        ck_url = CheckURL()
        verified_url_obj = ck_url.is_malicious(host=host, port=port, url=url_path, query_string=query_string)
        return verified_url_obj, 200
