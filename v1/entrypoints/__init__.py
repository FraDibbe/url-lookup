from flask import Blueprint
from flask_restplus import Api

# Create a blueprint that will link the API Engine to the App
apiBlueprint = Blueprint('api', __name__)

apiEngine = Api(apiBlueprint, version='1.0', title='Cisco Challenge',
                description='Url Checker Service', prefix="/urlinfo/1")



# Namespace Import
from .urlAPI import challenge

# clear all the namespaces
apiEngine.namespaces.clear()
# Add Namespaces (and all the routes own by the resources) to the ApiEngine
apiEngine.add_namespace(challenge)
