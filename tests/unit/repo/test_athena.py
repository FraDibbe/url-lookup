import os
import unittest
from unittest import mock

from flask.config import Config

from v1.repo.AthenaStorage import AthenaStorage


class TestAthenaStorage(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Read the same configuration used by flask
        flask_dir = os.path.dirname(os.path.realpath(__file__))
        config_reader = Config(flask_dir)
        cls.config = config_reader
        config_reader.from_object('config')

        # Create an instance of Athena
        cls.athena = AthenaStorage()

        # Init Athena DB
        cls.athena.init_db(db_name=config_reader['ATHENA_DATABASE_NAME'],
                           connection_string=config_reader['ATHENA_CONNECTION_STRING'],
                           s3_source=config_reader['ATHENA_S3_SOURCE'],
                           s3_output=config_reader['ATHENA_S3_OUTPUT']
                           )


    @mock.patch('v1.repo.AthenaDataBase.AthenaDataBaseAPI.run_query', result_value={'abc': 'lol'})
    def test_retrieve_malware_info(self, run_query_mock):
        malicious_url = '/home/steal-money'
        malicious_host = '192.45.168.2'
        malicious_port = '80'
        resp = self.athena.is_malicious(malicious_host, malicious_port, malicious_url)
        run_query_mock.assert_called()
        self.assertIsNotNone(resp)

    def test_build_malicious_build_query(self):
        malicious_url = '/home/steal-money'
        malicious_host = '192.45.168.2'
        malicious_port = '80'
        malicious_query_params = 'param1=abc&parm2=def'
        sql = self.athena._build_check_malware_SQL(malicious_host, malicious_port, malicious_url,
                                                   malicious_query_params)
        exp_sql = "SELECT COUNT(*) as Count FROM malware WHERE hostname ='192.45.168.2' " \
                  "AND port ='80' AND url='/home/steal-money' AND query_param='param1=abc&parm2=def';"
        self.assertEqual(exp_sql, sql)
        sql2 = self.athena._build_check_malware_SQL(malicious_host, malicious_port, malicious_url)
        exp_sql2 = "SELECT COUNT(*) as Count FROM malware WHERE hostname ='192.45.168.2' " \
                    "AND port ='80' AND url='/home/steal-money';"
        self.assertEqual(exp_sql2, sql2)

    def test_database_type(self):
        self.assertEqual(AthenaStorage, self.athena.get_database_type())


if __name__ == '__main__':
    unittest.main()
