import os
import unittest
from unittest import mock

from flask.config import Config

from v1.repo.AthenaDataBase import AthenaNotInitializedError, AthenaDataBaseAPI


class TestAthenaDBAPI(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Read the same configuration used by flask
        flask_dir = os.path.dirname(os.path.realpath(__file__))
        config_reader = Config(flask_dir)
        cls.config = config_reader
        config_reader.from_object('config')

        # Create an instance of AthenaDataBaseAPI
        cls.athena = AthenaDataBaseAPI()

        # Init Athena DB
        cls.athena.init_db(db_name=config_reader['ATHENA_DATABASE_NAME'],
                           connection_string=config_reader['ATHENA_CONNECTION_STRING'],
                           s3_source=config_reader['ATHENA_S3_SOURCE'],
                           s3_output=config_reader['ATHENA_S3_OUTPUT']
                           )

    def test_is_storage_initialized(self):
        self.assertIsNotNone(self.athena.db)
        self.assertIsNotNone(self.athena._connection_string)
        self.assertIsNotNone(self.athena.s3_output)
        self.assertIsNotNone(self.athena.s3_source)

    @unittest.skip
    def test_try_to_reinit_storage(self):
        """
        Double initialization, in the setUpClass and here, it should rise an assertion
        : ONY ONE INSTANCE PER APPLICATION (SINGLETON CLASS)
        """
        self.assertRaises(AthenaNotInitializedError, self.athena.init_db,
                          self.config['ATHENA_DATABASE_NAME'],
                          self.config['ATHENA_CONNECTION_STRING'],
                          self.config['ATHENA_S3_SOURCE'],
                          self.config['ATHENA_S3_OUTPUT']
                          )

    @mock.patch('v1.repo.AthenaDataBase.AthenaDataBaseAPI._AthenaDataBaseAPI__wait_till_completed')
    def test_run_query(self, mock_wait_till_completed):
        # MOCK the Boto Client instance
        boto_client = mock.Mock()
        boto_client.start_query_execution.return_value = {'QueryExecutionId': 'FAKE_ID_12344'}
        # monkey-patch self.athena
        self.athena._AthenaDataBaseAPI__client = boto_client
        # set return value for function
        mock_wait_till_completed.result_value = 'some result'
        # Execute patched code
        self.athena.run_query("SELECT * FROM fake_table;")
        boto_client.start_query_execution.assert_called()

    def test_build_lst_dict_from_raw_rows(self):
        athena = AthenaDataBaseAPI()
        example_raw_rows = [{'Data': [{'VarCharValue': 'hostname'}, {'VarCharValue': 'port'}, {'VarCharValue': 'url'},
                                      {'VarCharValue': 'query_param'}]},
                            {'Data': [{'VarCharValue': '1.1.1.1'}, {'VarCharValue': '1000'},
                                      {'VarCharValue': '/home'}, {'VarCharValue': 'param1=1&param2=67'}]}]
        example_keys_set = {'hostname', 'port', 'url', 'query_param'}
        returned_lst_dict = athena.build_lst_of_dict(example_raw_rows, example_keys_set)
        expected_lst_dict = [
            {'hostname': '1.1.1.1', 'port': '1000', 'url': '/home', 'query_param': 'param1=1&param2=67'},
        ]
        self.assertEqual(len(expected_lst_dict), len(returned_lst_dict))
        self.assertListEqual(expected_lst_dict, returned_lst_dict)

    def test_database_type(self):
        athena = AthenaDataBaseAPI()
        self.assertEqual(AthenaDataBaseAPI, athena.get_database_type())


if __name__ == '__main__':
    unittest.main()
