import json
import unittest
from unittest import mock

from v1 import app as app_factory
from v1.domain.VerifiedURL import VerifiedURL


class TestURLApi(unittest.TestCase):
    BASE_URL = 'urlinfo/1'

    @classmethod
    def setUpClass(cls):
        """
        Ensures that a new application and new database is created at each test function run
        """
        with mock.patch('v1.repo.AthenaStorage.AthenaStorage.create_db') as create_db_meth:
            with mock.patch('v1.repo.AthenaStorage.AthenaStorage.create_tables') as create_tabl_meth:
                cls.app = app_factory.create_app('config')
                create_db_meth.assert_called()
                create_tabl_meth.assert_called()
        # Establish an application context before running the tests.
        cls.ctx = cls.app.app_context()
        cls.ctx.push()
        cls.testclient = cls.app.test_client()

    def decode_json(self, response):
        # decode JSON response and load as Dict
        return json.loads(response.data.decode('utf-8'))

    @classmethod
    def tearDownClass(cls):
        """
        Ensures that the application context is popped out
        """
        cls.ctx.pop()

    @mock.patch('v1.entrypoints.urlAPI.CheckURL.is_malicious', return_value=VerifiedURL('1', '1', '1', malicious=False))
    def test_check_safe_url(self, check_url_is_malicious):
        rv = self.testclient.get(self.BASE_URL + '/192.34.11.129:80/home')
        check_url_is_malicious.assert_called()
        self.assertEqual(rv.status_code, 200)
        json_rv = self.decode_json(rv)
        self.assertTrue('malicious' in json_rv)
        self.assertEqual(False, json_rv['malicious'])

    @mock.patch('v1.entrypoints.urlAPI.CheckURL.is_malicious', return_value=VerifiedURL('1', '1', '1', '1', False))
    def test_check_safe_url_query_string(self, check_url_is_malicious):
        rv = self.testclient.get(self.BASE_URL + '/192.34.11.129:80/home?str=567')
        check_url_is_malicious.assert_called()
        self.assertEqual(rv.status_code, 200)
        json_rv = self.decode_json(rv)
        self.assertTrue('malicious' in json_rv)
        self.assertEqual(False, json_rv['malicious'])

    @mock.patch('v1.entrypoints.urlAPI.CheckURL.is_malicious', return_value=VerifiedURL('1', '1', '1', malicious=True))
    def test_malicious_url(self, check_url_is_malicious):
        rv = self.testclient.get(self.BASE_URL + '/127.0.0.1:80/home/house')
        check_url_is_malicious.assert_called()
        self.assertEqual(rv.status_code, 200)
        json_rv = self.decode_json(rv)
        self.assertTrue('malicious' in json_rv)
        self.assertEqual(True, json_rv['malicious'])

    @mock.patch('v1.entrypoints.urlAPI.CheckURL.is_malicious', return_value=VerifiedURL('1', '1', '1', '1', True))
    def test_malicious_url_query_string(self, check_url_is_malicious):
        rv = self.testclient.get(self.BASE_URL + '/1.1.1.1:1000/home?param1=1&param2=67')
        check_url_is_malicious.assert_called()
        self.assertEqual(rv.status_code, 200)
        json_rv = self.decode_json(rv)
        self.assertTrue('malicious' in json_rv)
        self.assertEqual(True, json_rv['malicious'])

    def test_request_no_params(self):
        rv = self.testclient.get(self.BASE_URL + '/1.1.1.1:1000')
        self.assertEqual(rv.status_code, 404)
        rv = self.testclient.get(self.BASE_URL + '/')
        self.assertEqual(rv.status_code, 404)

    def test_request_bad_format_url(self):
        rv = self.testclient.get(self.BASE_URL + '/1.1.1.1::1000/url_very()/crazy7')
        self.assertEqual(rv.status_code, 400)


if __name__ == '__main__':
    unittest.main()
