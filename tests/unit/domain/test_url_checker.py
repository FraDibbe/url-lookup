import unittest
from unittest import mock

from v1.domain.CheckURL import CheckURL
from v1.repo.AthenaStorage import AthenaStorage


class TestCheckURL(unittest.TestCase):

    @mock.patch('v1.domain.CheckURL.athena', return_value=AthenaStorage())
    def test_url_is_safe(self, athena_mock):
        url_checker = CheckURL()
        athena_mock.is_malicious.return_value = False
        verifiedUrl = url_checker.is_malicious('1.1.1.2', '80', '/home')
        athena_mock.is_malicious.assert_called_once_with('1.1.1.2', '80', '/home', None)
        self.assertEqual(False, verifiedUrl.malicious)

    @mock.patch('v1.domain.CheckURL.athena', return_value=AthenaStorage())
    def test_url_is_malicious(self, athena_mock):
        url_checker = CheckURL()
        athena_mock.is_malicious.return_value = True
        verifiedUrl = url_checker.is_malicious('1.1.1.1', '80', '/home', 'abab=123&aafaf=889&ajaj=45regd')
        athena_mock.is_malicious.assert_called_once_with('1.1.1.1', '80', '/home',
                                                                      'abab=123&aafaf=889&ajaj=45regd')
        self.assertEqual(True, verifiedUrl.malicious)


if __name__ == '__main__':
    unittest.main()
