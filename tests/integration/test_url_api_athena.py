import unittest

from tests.integration.BaseUnittestClass import TestFlaskApp


class TestURLApi(TestFlaskApp):

    def test_check_safe_url_query_string(self):
        rv = self.testclient.get(self.BASE_URL + '/192.34.11.129:80/home?str=567')
        self.assertEqual(rv.status_code, 200)
        json_rv = self.decode_json(rv)
        self.assertTrue('malicious' in json_rv)
        self.assertEqual(False, json_rv['malicious'])

    def test_malicious_url_query_string(self):
        rv = self.testclient.get(self.BASE_URL + '/1.1.1.1:1000/home?param1=1&param2=67')
        self.assertEqual(rv.status_code, 200)
        json_rv = self.decode_json(rv)
        self.assertTrue('malicious' in json_rv)
        self.assertEqual(True, json_rv['malicious'])


if __name__ == '__main__':
    unittest.main()
