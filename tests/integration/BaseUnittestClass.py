import json
import unittest

from v1 import app as app_factory


class TestFlaskApp(unittest.TestCase):
    BASE_URL = 'urlinfo/1'

    @classmethod
    def setUpClass(cls):
        """
        Ensures that a new application and new database is created at each test function run
        """
        cls.app = app_factory.create_app('config')

        # Establish an application context before running the tests.
        cls.ctx = cls.app.app_context()
        cls.ctx.push()
        cls.testclient = cls.app.test_client()

    def decode_json(self, response):
        # decode JSON response and load as Dict
        return json.loads(response.data.decode('utf-8'))

    @classmethod
    def tearDownClass(cls):
        """
        Ensures that the application context is popped out
        """
        cls.ctx.pop()

