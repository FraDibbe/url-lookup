import os
import unittest

from flask.config import Config

from v1.repo.AthenaStorage import AthenaStorage


class TestAthenaStorage(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Read the same configuration used by flask
        flask_dir = os.path.dirname(os.path.realpath(__file__))
        config_reader = Config(flask_dir)
        cls.config = config_reader
        config_reader.from_object('config')

        # Create an instance of Athena
        cls.athena = AthenaStorage()

        # Init Athena DB
        cls.athena.init_db(db_name=config_reader['ATHENA_DATABASE_NAME'],
                           connection_string=config_reader['ATHENA_CONNECTION_STRING'],
                           s3_source=config_reader['ATHENA_S3_SOURCE'],
                           s3_output=config_reader['ATHENA_S3_OUTPUT']
                           )

        # make sure that the schema exists before each test method
        cls.athena.create_db()
        cls.athena.create_tables(config_reader['ATHENA_TABLE_MAPPING_DICT'])

    def test_retrieve_malware_info_with_query_parms_INTEGRATION(self):
        malicious_url = '/home'
        malicious_host = '1.1.1.1'
        malicious_port = '1000'
        malicious_query_p = 'param1=1&param2=67'
        resp = self.athena.is_malicious(malicious_host, malicious_port, malicious_url, malicious_query_p)
        self.assertEqual(True, resp)

    def test_retrieve_malware_info_INTEGRATION(self):
        urls_list = [
            ({'host': '127.0.0.2',
             'port': '5000',
             'url': '/welcome',
             'query_params': 'steal=true'
             }, True),
            ({'host': '178.12.1.6',
             'port': '80',
             'url': '/my_safe_website'
             }, False),
            ({'host': '127.0.0.1',
             'port': '80',
             'url': '/home/house'
             }, True),
        ]
        for data_to_verify,expected_res in urls_list:
            resp = self.athena.is_malicious(**data_to_verify)
            self.assertEqual(expected_res, resp)

    def test_request_bad_format_url(self):
        pass

    def test_database_type(self):
        athena = AthenaStorage()
        self.assertEqual(AthenaStorage, athena.get_database_type())


if __name__ == '__main__':
    unittest.main()
