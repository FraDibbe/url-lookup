import v1.app as app_factory

"""
Entry point for VimCar application
"""

configuration = ['config']

app = None
for conf in configuration:
    app = app_factory.create_app(conf)

if __name__ == "__main__":
        #global app
        app.run(host='0.0.0.0', port=80)