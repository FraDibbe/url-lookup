# Cisco  -- Developer Challenge

We have an HTTP proxy that is scanning traffic looking for malware URLs. Before allowing HTTP connections to be made, this proxy asks a service that maintains several databases of malware URLs if the resource being requested is known to contain malware.

Write a small web service, in the language/framework your choice, that responds to GET requests where the caller passes in a URL and the service responds with some information about that URL. The GET requests look like this:
`GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}`
The caller wants to know if it is safe to access that URL or not. As the implementer, you get to choose the response format and structure. These lookups are blocking users from accessing the URL until the caller receives a response from your service.

Extra points are the service is containerized (Docker). Extra points if you host the service somewhere so we can test it out. But prefer quality over quantity.

Give some thought to the following:

The size of the URL list could grow infinitely; how might you scale this beyond the memory capacity of where you're running it now? Bonus if you implement this. The number of requests may exceed the capacity of the system you're running it on, how might you solve that? Bonus if you implement this. What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every 10 minutes.

Email us your ideas.

#### Project Sources
The solution can be found here: https://bitbucket.org/FraDibbe/url-lookup/src/master/

##### Stack
Python3.6, Flask, Restplus, AWS Boto3, Docker, Apache HTTPd, Kubernetes, AWS Athena, AWS S3, Google Cloud Platform

### Project description and implementation
The core part of the project is a *lookup service*, which given information like hostname, port, URL - plus optionally - query string params, will output if this combination is recognized as a Malicious URL or not.
Simply put, a hashmap or a database table on any NoSQL (or SQL) databases will do the job pretty easily.
This solution, however, was *build with scalability in mind* and if course to reach this goal, apart from an exception design, we must use **Cloud Services**.

The minimal web service is power by Flask, as we are going to use a hosted  (and managed) DB we don't need any ORM - or at least we don't have any already made, hence most of the effort in this project was made to **get Amazon Athena integrated with Flask**, while providing at the same time a data access abstraction layer building (check folder `/v1/repo`)

Why Athena for the backend?
Well, although is a pretty slow database mostly used for Analitycs it gives as a very power SQL interface to query data directly on S3!
Using S3 database is one of the possible answers to the question 
> The size of the URL list could grow infinitely; how might you scale this beyond the memory capacity of where you're running it now?

Of course, there are different hosted services like Redshift or DynamoDB on AWS which can perform faster, but they require higher managed cost, more time to set up and they don't specifically rely on S3.
Having S3 as **infinitely scalable and managed** storage will help us to answer to question 

> What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every 10 minutes.

Answers are explored in details in the section below.

*Clean Architecture* is a style to build an application, which is incredibly suitable for Web Services as it introduces a nice separation between domain concepts and framework layers.
Dependencies only point inward, basically, no domain object depends on the framework and the DBs (plus other infrastructure elements) are exposing interfaces to the entrypoints (i.e. CLI, REST or Graphical frameworks)
Domain is the core, the rest is all around and can be easily swapped.

In this application, we have just one concept, namely the VerifiedURL, which is simply a *Value Object* as no logic is implemented. As Use Case (or Interactor) we have the CheckURL Service (again in domain folder). 

The DB is abstracted by the `AthenaStorage` class, which in turn uses the "low level" library `AthenaDataBaseAPI` class. Both of them were written for this project and they are "Clean Architecture" wrappers on top of AWS boto3 lib.

The entrypoint has a REST resource which uses the DB and the Use Case Service

The rest framework used in the project is Flask-Restplus which give "for free" a web interface, describing all the endpoints and give the Docs for them. Unfortunately, because of built-in input sanitization, isn't possible to try correctly the endpoint from the web interface.

**On the deployment** side, after the tests were passing, it was time to leave the Flask Deployment server and to use Apache HTTPd running on a docker container.
A Docker file is provided, follow the appropriate section to run the container on local machine.
Finally, the service, containerized, can be deployed on the Internet.
To ease the deployment operations and to (partially) answer to the question:

> The number of requests may exceed the capacity of the system you're running it on, how might you solve that? Bonus if you implement this.

I deployed the service on 4 Node cluster on Kubernetes. The deployment is described by 2 files in `./kubernetes/` folder and the **service is live** at:

`http://35.242.229.145`

Examples:
`http://35.242.229.145/urlinfo/1/1.1.1.1:1000/home/yuuu`   -> tells `1/1.1.1.1:1000/home/yuuu` is not malicious
`http://35.242.229.145/urlinfo/1/1.1.1.1:1000/home?param1=1&param2=67` -> tells `1.1.1.1:1000/home?param1=1&param2=67` is malicious

Data structure used,a very simple JSON:

```python
{
'url_path': "Requested_URL",
'malicious': True/False
}
```

### How to run the project

First of all, S3 and Athena services were created on Amazon WS console, along with User implementing the right policies via the IAM service. The permission granted to this user permitted to write and read using Athena and S3.

Note: In S3 there is a small data set, containing the malicious URLs. In the repo it is `malware.csv` file. 

When Athena is activated, we must upload CSV files to an S3 bucket, in this way Athena is able to query the bucket and scan all the files contained in it, while returning SQL structured data.

Kubernetes must be configured too, so a cluster must be created and then using Google Cloud Shell we must deploy YAML files (Deployment and Load Balancer) and verify their status.

To run it locally, without Docker, simply:

- create a virtual environment
- add the dependencies using the `requirements.txt` file provided
- when the environment (python >= 3.5) is ready move, via command line, to :
    `cisco-url-lookup`
- type:
    `python runserver.py`
    
this will start the developer server locally, on localhost:5000. `config.py` is the file where AWS secrets and access keys are specified. In this version secrets are in Plain text, the best way is to pass them as SECRET ENVIRONMENT VARIABLES, that creates a docker secret (and a kubernetes secret, later on) and make python read from os.env.
The config file already prefers os.env over statically typed AWS keys, but the docker and kubernetes files don't specify any secret yet.

To run it on docker, just use the Dockerfile provided and type:

```sh
docker build -t cisco-webserver .
docker run -d -p80:80 cisco-webserver
```
or simply 
```sh
docker pull fradiben/cisco-webserver
```
from Docker Hub public repo.

To run it from the cloud, just use the load balancer public IP: `http://35.242.229.145`

you can try it on the browser or use cURL or Postman or any HTTP client.

```sh
curl -X GET "http://35.242.229.145/urlinfo/1/1.1.1.1:1000/home?param1=1&param2=67"
```

#### How to tests the application

Tests are found in the folder `./tests`

There are 2 types of tests, unit and integration.
Unittests are quicker and they test units in isolation, using mocking to mock the framework and/or Athena DB.

To run the unit tests, type:

`python -m unittest discover tests.unit -v`

```
(venv) C:\Users\...\url-lookup>python -m unittest discover tests.unit -v
test_url_is_malicious (tests.unit.domain.test_url_checker.TestCheckURL) ... ok
test_url_is_safe (tests.unit.domain.test_url_checker.TestCheckURL) ... ok
Sono entrato nell app
test_check_safe_url (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_check_safe_url_query_string (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_malicious_url (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_malicious_url_query_string (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_request_bad_format_url (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_request_no_params (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_build_malicious_build_query (tests.unit.repo.test_athena.TestAthenaStorage) ... ok
test_database_type (tests.unit.repo.test_athena.TestAthenaStorage) ... ok
test_retrieve_malware_info (tests.unit.repo.test_athena.TestAthenaStorage) ... ok
test_build_lst_dict_from_raw_rows (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok
test_database_type (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok
test_is_storage_initialized (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok
test_run_query (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok
test_try_to_reinit_storage (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok

----------------------------------------------------------------------
Ran 16 tests in 0.189s

OK
```

to run the integration tests type:

`python -m unittest discover tests.integration -v`

```
(venv) C:\Users\...\url-lookup>python -m unittest discover tests.integration -v
test_database_type (tests.integration.test_athena_integration.TestAthenaStorage) ... ok
test_request_bad_format_url (tests.integration.test_athena_integration.TestAthenaStorage) ... ok
test_retrieve_malware_info_INTEGRATION (tests.integration.test_athena_integration.TestAthenaStorage) ... ok
test_retrieve_malware_info_with_query_parms_INTEGRATION (tests.integration.test_athena_integration.TestAthenaStorage) ... ok
test_check_safe_url_query_string (tests.integration.test_url_api_athena.TestURLApi) ... ok
test_malicious_url_query_string (tests.integration.test_url_api_athena.TestURLApi) ... ok

----------------------------------------------------------------------
Ran 6 tests in 20.692s

OK
```

to run all together type:

`python -m unittest -v`

```
(venv) C:\Users\...\url-lookup>python -m unittest -v
test_database_type (tests.integration.test_athena_integration.TestAthenaStorage) ... ok
test_request_bad_format_url (tests.integration.test_athena_integration.TestAthenaStorage) ... ok
test_retrieve_malware_info_INTEGRATION (tests.integration.test_athena_integration.TestAthenaStorage) ... ok
test_retrieve_malware_info_with_query_parms_INTEGRATION (tests.integration.test_athena_integration.TestAthenaStorage) ... ok
test_check_safe_url_query_string (tests.integration.test_url_api_athena.TestURLApi) ... ok
test_malicious_url_query_string (tests.integration.test_url_api_athena.TestURLApi) ... ok
test_url_is_malicious (tests.unit.domain.test_url_checker.TestCheckURL) ... ok
test_url_is_safe (tests.unit.domain.test_url_checker.TestCheckURL) ... ok
test_check_safe_url (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_check_safe_url_query_string (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_malicious_url (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_malicious_url_query_string (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_request_bad_format_url (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_request_no_params (tests.unit.entrypoints.test_url_api.TestURLApi) ... ok
test_build_malicious_build_query (tests.unit.repo.test_athena.TestAthenaStorage) ... ok
test_database_type (tests.unit.repo.test_athena.TestAthenaStorage) ... ok
test_retrieve_malware_info (tests.unit.repo.test_athena.TestAthenaStorage) ... ok
test_build_lst_dict_from_raw_rows (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok
test_database_type (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok
test_is_storage_initialized (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok
test_run_query (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok
test_try_to_reinit_storage (tests.unit.repo.test_athena_db_api.TestAthenaDBAPI) ... ok

----------------------------------------------------------------------
Ran 22 tests in 20.793s

OK
```

NOTE: virtual environment must be active and correctly set up


###### Answers to: "The size of the URL list could grow infinitely; how might you scale this beyond the memory capacity of where you're running it now? Bonus if you implement this"

This question is the fundamental reason why the application is relying on Cloud Service. 

- URL list could grow infinitely -> there is no better MANAGED and simple storage as S3. From this consideration, Athena come along pretty straightforward the only SQL serverless service which can directly query S3 datastore

If we want to use managed SQL or NoSQL, we can use Redshift, DynamoDB to perform traditional queries. If we prefer fast access, sacrificing a the URL list growth, we can implement all of this in a key store database like Redis!

###### Answers to: "The number of requests may exceed the capacity of the system you're running it on, how might you solve that? Bonus if you implement this"
 
The main answer is to have a cluster of Kubernetes. The advantage is pretty straightforward: 
1. managing of scaling and load balancing between multiple replicas.  
2. physical node management at ease with google shell and kubernets UI to manage node creation  

In order to speed up response to queries a caching system must be used, basically, imagine multiple users asking for the same URL over and over again, a cache like Redis would speed up the - quite slow - Athena, and save us lots of costly queries. The cache can be refreshed every 5mins, saving up lots and lots load to the services and to Athena.

If the traffic goes crazy, like >= 3k requests per minute, a queue system between the web service and Athena can be implemented to buffer the requests and have workers take care of sending responses to the user.

###### Answers to: " What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every 10 minutes"

S3 is the perfect storage for this kind of problem, in fact, it can store cheaply an infinite amount of data, but it is slow. 10 min between each update is a good delay for S3. Normally S3 access times are around minutes.

To manage the upload to S3 we can use Amazon Firehouse data pipeline to easily upload lots and lots of data to S3 (this removes the buffer-queue problem) and/or deploy another microservice, which will expose one endpoint. This endpoint will be used only to upload the URL list to the could (as JSON), using firehose libs or directly s3 libs.
This microservice must be accessible at the same public URL of the whole service, let imagine it is still `http://35.242.229.145`. Is possible to define a rule in the Load Balancers (as well scalable and managed too by GCP) to redirect requests to `http://35.242.229.145/upload` to the second microservice and `http://35.242.229.145/urlinfo/` to the already developed service. 
In this way, we can serve more throughput than having one 1 service with 2 endpoints.

#### OpenAPI (Swagger) URL map

-  /: At this route (with a browser) you can see the documentation of each Endpoint and test all of them
-  GET /urlinfo/1/<hst:port>/<url?query>: Is the GET endpoint, unfortunately, the input sanitizing lib converts ':' , '/' and '?' the symbol in their HTTP escaped chars so calling the endpoint from swagger doesn't really work. Is still good for documentation, anyway.


### Author

Francesco Di Benedetto - fra.dibenedetto1@gmail.com 
Sorry for typos, if any :)