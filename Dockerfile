FROM fradiben/centos_apache

# setup apache default wsgi vhost
COPY apache_virtual_server/cisco.conf /etc/httpd/conf.d/welcome.conf

# setup docker run script 
COPY apache_virtual_server/run-apache-httpd.sh /run-apache-httpd.sh
RUN chmod -v +x /run-apache-httpd.sh

# change the working directory, here we will copy all the files for the app to workdock
WORKDIR /var/www/html

# Copy project files
COPY ./v1  ./v1
COPY ["runserver_docker.py", "config.py", "cisco.wsgi", "./"]
RUN mv ./runserver_docker.py ./runserver.py

# pip copy and install App requirements
COPY ./requirements.txt .

RUN pip3.5 install --upgrade pip; pip3.5 install --no-cache-dir -r requirements.txt


# Change the file permissions
RUN chown apache:apache -R ./ ;
RUN find . -type f -exec chmod 0644 {} \;
RUN find . -type d -exec chmod 0755 {} \;

EXPOSE 80

# run flask app on apache
CMD ["/run-apache-httpd.sh"]


#docker build -t fradiben/vimcar .
