from v1 import app as app_factory

configuration = ['config']

for conf in configuration:
    app = app_factory.create_app(conf)
    app.run(port=5000)
