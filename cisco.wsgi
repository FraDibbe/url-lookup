#!/usr/bin/python
import sys, logging, os

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/html")
os.chdir("/var/www/html/v1")

from runserver import app as application
